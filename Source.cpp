#include<opencv2\opencv.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>


#include<stdlib.h>
namespace po = boost::program_options;
using namespace cv;
using namespace std;
IplImage* image;
int main(int argc, char* argv[])
{
   std::string settings_type;
	

	int Nx, Ny;
	// Setup options.
	po::options_description desc("Options");
	desc.add_options()
		("n.x", po::value< int>(&Nx), "settings_type_x")
		("n.y", po::value< int>(&Ny), "settings_type_y")
		("imagename.name", po::value<string>(&settings_type), "settings_type_image_name");

	// Load setting file.
	po::variables_map vm;
	std::ifstream settings_file("settings.ini", std::ifstream::in);
	po::store(po::parse_config_file(settings_file, desc), vm);
	settings_file.close();
	po::notify(vm);
	//std::cout << "settings.type: " << settings_type << std::endl;
	/*int Nx = atoi(settings_type_x.c_str());
	int Ny = atoi(settings_type_y.c_str());*/
	char *settings_type_name=new char[settings_type.length()+1];
    strcpy(settings_type_name, settings_type.c_str());
	cout << Nx << " " << Ny << " " << settings_type;
	
	char* filename = argc == 2 ? argv[1] : settings_type_name;
	//image = imread(filename, 0);
	Mat	A = imread(filename, 0);
	float t;
	Mat B(A.rows, A.cols, CV_32FC1);
	//image = cvLoadImage(filename, 1);
	for (int i = 0; i < A.rows; i++) {
		for (int j = 0; j < A.cols; j++) {
			B.at<float>(i, j) = (float)A.at<uchar>(i, j);
			//B.data[i*B.step + j*B.elemSize()]=(float)A.data[i*A.step + j*A.elemSize()];
		}
	}

	//for (int i = 0; i < B.rows-45; i++) {
	//	for (int j = 0; j < B.cols-45; j++) {
	//		//B.data[i*B.step + j*B.elemSize()] = (float)A.data[i*A.step + j*A.elemSize()];
	//		//printf("%d\n ", &A.data[i*A.step + j*A.elemSize()]);
	//		cout << B.at<float>(i,j) << " ";

	//	}cout << endl;
	//}
	
	Mat C(B.rows*Ny-Ny+1, B.cols*Nx-Nx+1, CV_32FC1);

	for (int i = 0; i < C.rows; i++) {
		for (int j = 0; j < C.cols; j++) {
			C.at<float>(i, j) = 100;
		}
	}

	for (int i = 0; i < B.cols; i++) {
		for (int j = 0; j < B.rows; j++) {
			C.at<float>(i*Ny, j*Nx) = B.at<float>(i, j);
		}
	}
	//rastyashenie strok
	for (int j = 0; j < B.rows; j++) {
		int de = 1;
		for (int i = 1; i < B.cols; i++) {
			float stepX;
			stepX = (B.at<float>(j, i) - B.at<float>(j, i - 1)) / (Nx);
			for (int k = 1; k < Nx; k++) {
				C.at<float>(j*Ny, de) = B.at<float>(j, i - 1) + stepX*k;
				de++;
			}
			de++;
		}
	}
	/*for (int j = 0; j < B.cols; j++) {
		int de = 1;
		for (int i = 1; i < B.rows; i++) {
			float stepY;
			stepY = (B.at<float>(i, j) - B.at<float>(i - 1, j)) / (Ny);
			for (int k = 1; k < Ny; k++) {
				C.at<float>(de, j*Nx) = B.at<float>(i - 1, j) + stepY*k;
				de++;
			}
			de++;
		}
	}*/
	for (int j = 0; j < C.cols; j++) {
		int de = 1;
		for (int i = 1; i < B.rows; i++) {
			float stepY1 = (abs(C.at<float>(i*Ny, j)) - abs(C.at<float>((i - 1)*Ny, j))) / Ny;
			for (int k = 1; k < Ny; k++) {
				C.at<float>(de, j) = C.at<float>((i - 1)*Ny, j) + stepY1*k;
				de++;
			}
			de++;
		}
	}
	/*for (int j = 0; j < C.cols; j++) {
		int de = 1;
		for (int i = 1; i < B.rows; i++) {
			float stepY;
			stepY = (C.at<float>(i*Ny, j) - C.at<float>((i - 1)*Ny, j)) / (Ny);
			for (int k = 1; k < Ny; k++) {
				C.at<float>(de, j) = C.at<float>(i - 1, j) + stepY*k;
				de++;
			}
			de++;
		}
	}*/




	/*float stepX, stepY;
	for (int i = 0; i < B.rows; i++) {
		for (int j = 1; j < B.cols; j++) {

			stepX = (B.at<float>(i, j) - B.at<float>(i, j - 1)) / Nx;
			int r = B.at<float>(i, j - 1);
			for (int h = 1; h < Nx; h++) {
				C.at<float>(i*Nx, Nx * (j - 1) + h) = r + stepX;
				r = r + stepX;
			}
		}
	}
	*/




	//for (int i = 0; i <C.cols; i++) {
	//	for (int j = 1; j < B.rows; j++) {
	//		stepY = (C.at<float>(j*Ny, i) - C.at<float>((j - 1)*Ny, i)) / Ny;
	//		//cout << stepY << " ";
	//		int r = C.at<float>((j - 1) * Ny, i);
	//		for (int h = 1; h < Ny; h++) {
	//			C.at<float>(Ny * (j - 1) + h, i) = abs(r + stepY);
	//			r = r + stepX;
	//		}
	//	}
	//}

//for (int i = 0; i < B.cols; i++) {
//		for (int j = 1; j < B.rows; j++) {
//
//			stepX = (B.at<float>(j, i) - B.at<float>(j-1, i)) / Nx;
//			int r = B.at<float>(j-1, i);
//			for (int h = 1; h < Ny; h++) {
//				C.at<float>(Nx * (j - 1) + h, i*Nx) = (r + stepX);
//				r = r + stepX;
//			}
//		}
//	}

	//for (int i = 0; i < 10; i++) {
	//	for (int j = 0; j < 10; j++) {
	//		//B.data[i*B.step + j*B.elemSize()] = (float)A.data[i*A.step + j*A.elemSize()];
	//		//printf("%d\n ", &A.data[i*A.step + j*A.elemSize()]);
	//		cout << C.at<float>(i, j) << " ";

	//	}cout << endl;
	//}
	imwrite("Result.jpg", C);
	/*namedWindow("Display window", WINDOW_AUTOSIZE);
	imshow("Display window", C);*/
	//system("pause");
	
}